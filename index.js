const express = require('express');
const bodyParser = require('body-parser');
const DB = require('./services/vetdb');
const telegram = require('./services/botHandler').botHandler;
const Logs = require('./services/logger').Log;
const MES = require('./services/messages').pack;
const path = require('path');
require('dotenv').config();

// Control Variables
let PORT = 3927;
let API = '/api/hooks';

const S3 = process.env.S3;

const app = express();
app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, '/html')));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

const routes = {
	default:'/',
	bitbucket: { // Hook Routes for Bitbucket
		anabot: `${API}/bitbucket/anabot/repo`,
		generic: `${API}/bitbucket/repo-update`
	},
	ifttt: { // Hook Routes for IFTTT
		jemCalendar: `${API}/ifttt/google-calendar/update`,
		weather: `${API}/ifttt/weather/daily/update`,
		PortMania: {
			entry: `${API}/ifttt/port-mania/entry`,
			exit: `${API}/ifttt/port-mania/exit`
		}
	},
	museagram: { // Museagram Hook Routes
		getLogs: `${API}/museagram/webhooks/get-logs`,
		getWeather: `${API}/museagram/webhooks/get-weather`,
		sendMessage: `${API}/museagram/:id/transfer/message-send`
	},
	vetinventory: {
		insertMany: `${API}/vetinventory/product/insert-many`,
		insertOne: `${API}/vetinventory/product/insert-one`,
		getItem: `${API}/vetinventory/product/does-exist`,
		getAllItems: `${API}/vetinventory/product/get-all`,
		updateItem: `${API}/vetinventory/product/update-item`,
		deleteItem: `${API}/vetinventory/product/delete-item`,
		getRecent: `${API}/vetinventory/product/get-recent`,
		getCount: `${API}/vetinventory/product/get-count`,
		getIncompleteCount: `${API}/vetinventory/product/get-incomplete-count`
	},
	anabot: { // Hook Routes for Ana
		sendMessage: `${API}/anabot/admin/run/send-message`,
		getCommands: `${API}/anabot/admin/run/show-commands`
	}
};

/******************

 Express Routes

 ******************/

app.get(routes.default, (req, res) => {
	res.sendFile('html/index.html');
});

// Museagram  Hooks
app.get(routes.museagram.getLogs, (req, res) => {
	res.status(200).send({status: 'success', data: Logs.apiRecords});
});

app.post(routes.museagram.sendMessage, (req, res) => {
	if (req.params.id === process.env.GATE) {
		let contents = req.body;
		switch (contents.messageType) {
			case 'HTML':
				telegram.sendHTMLMessage(contents.target, contents.message);
				break
			case 'Markdown':
				telegram.sendMarkdownMessage(contents.target, contents.message);
				break
			default:
				telegram.sendMarkdownMessage(contents.target, contents.message);
				break
		}
		res.status(200).send({status: 'success'});
	} else {
		res.status(403).send({status: 'You do not have permission to do this'});
	}
});

// Bitbucket Hooks
app.post(routes.bitbucket.anabot, function(req, res){
	if (req.body.repository.name === 'AnaBot') {
		let pusher = req.body.actor.display_name;
		let changes = [];
		for (var i = 0; i < req.body.push.changes.length; i++) {
			changes.push('   - '+req.body.push.changes[i].new.target.message+'\n');
		}
		sendAdminsMessage(MES.bitbucketUpdate(req.body.repository.name,pusher,changes));
		res.status(200).send({status:'success'});
	}
	else {
		res.status(200).send({status:'success'});
	}
});

app.post(routes.bitbucket.generic, function(req, res){
	let bundle = {
		author: req.body.actor.display_name,
		repo: req.body.repository.name,
		link: req.body.repository.links.html.href,
		commits: req.body.push.changes[0].commits.map(commit => commit.message)
	};
	sendAdminsMessage(MES.bitbucketMessage(bundle));
	res.status(200).send({status:'success'});
});

// IFTTT Hooks

// New Event Added from Jemimah Calendar
app.post(routes.ifttt.jemCalendar, function(req,res) {
	sendAdminsMessage(MES.calendarEvent('Adventure Time Calendar',JSON.stringify(req.body)));
	res.status(200).send({status:'success'});
});

// Person just entered Port Mania
app.post(routes.ifttt.PortMania.entry, function(req,res){
	sendAdminsMessage(MES.locationEntry('Port Mania',JSON.stringify(req.body)));
	res.status(200).send({status:'success'});
});

// Daily Weather Update
let todayWeather = {};
app.post(routes.ifttt.weather, function(req, res) {
	todayWeather = req.body;
	telegram.sendAdventureGroupMessage(MES.weatherReport(todayWeather));
	res.status(200).send({status:'success'});
});

app.get(routes.museagram.getWeather, function(req, res) {
	res.status(200).send(todayWeather);
});

/************************************

	VETINVENTORY TEMP ROUTES

************************************/
app.get(routes.vetinventory.getAllItems, function (req, res) {
	let operation = DB.getAllEntries();
	operation.then(result => {
		res.status(200).send(result);
	})
	.catch(err => {
		res.status(200).send(err);
	})
})

app.post(routes.vetinventory.insertMany, function(req,res) {
	let pck = req.body;
	let operation = DB.insertMany(pck);
	operation.then(result => {
		if (result.success && result.data) {
			res.status(200).send(result.data.ops[0]);
		} else if (result.success) {
			result.data.then(foundData => {
				res.status(200).send(foundData);
			})
		} else {
			res.status(200).send({status: "Added new items"});
		}
	})
});

// When a new found item is being inserted
app.post(routes.vetinventory.insertOne, function(req,res) {
	let pck = req.body;
	let operation = DB.insertOne(pck);
	operation.then(result => {
		if (result.success && result.data) {
			res.status(200).send(result.data.ops[0]);
		} else if (result.success) {
			result.data.then(foundData => {
				res.status(200).send(foundData);
			})
		} else {
			res.status(200).send({status: "Added New Item"});
		}
	})
});

// Check if an item exists in our db already 
app.post(routes.vetinventory.getItem, function (req, res) {
	let pck = req.body;
	let operation = DB.doesExist(pck.id);
	operation.then(result => {
		if (result) {
			res.status(200).send(result);
		} else {
			res.status(200).send({status: "Not found"});
		}
	})
});

// Update an item in db
app.post(routes.vetinventory.updateItem, function (req, res) {
	let pck = req.body;
	let operation = DB.updateItem(pck.id, pck);
	operation.then(result => {
		if (result) {
			res.status(200).send(result);
		} else {
			res.status(200).send({status: 'Update Successful'});
		}
	})
})

app.post(routes.vetinventory.deleteItem, function (req,res) {
	let pck = req.body;
	let operation = DB.deleteItem(pck.id);
	operation.then(result => {
		if (result) {
			res.status(200).send(result);
		} else {
			res.status(200).send({status: 'Update Successful'});
		}
	})
})

app.get(routes.vetinventory.getRecent, function(req,res) {
	let operation = DB.getRecentEntries();
	operation.then(result => {
		if (result) {
			res.status(200).send(result);
		} else {
			res.status(200).send({status: 'Something went wrong when querying'});
		}
	})
})

app.get(routes.vetinventory.getCount, function (req, res) {
	let operation = DB.getItemCount();
	operation.then(result => {
		if (result) {
			res.status(200).send({count: result});
		} else {
			res.status(200).send({status: 'Could not get count'})
		}
	})
})

app.get(routes.vetinventory.getIncompleteCount, function (req, res) {
	let operation = DB.getIncompleteItemsCount();
	operation.then(result => {
		if (result) {
			res.status(200).send({count: result});
		} else {
			res.status(200).send({status: 'Could not get count'})
		}
	})
})





// -----------------------------------------------------------------------------


// Start Server
app.listen(PORT,function(){
	if (process.env.NODE_ENV !== 'prod') {
		console.log('[Museagram.webhooks] Server started on port: ',PORT);
	} else {
		telegram.sendMarkdownMessage(S3,MES.serverStart(PORT,'Webhooks Server'));
	}
});


// Helper functions -----------------------------
function sendAdminsMessage(msg){
	telegram.sendMarkdownMessage(S3,msg);
}



// Show meaningful error message
process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  if (process.env.NODE_ENV !== 'prod') {
		console.log('unhandledRejection', error.message);
	}
});
