

module.exports.pack = {
  serverStart: serverStart,
  bitbucketUpdate: bitbucketUpdate,
  calendarEvent: calendarEvent,
  locationEntry:locationEntry,
  weatherReport: weatherReport
}

/*
 * @params: port ? Int -> Port where Server is running
 */
 function serverStart(port,job){
   return `*${job}* - Server started on ${port}`;
 }

/*
 * @params: repo    ? String -> Name of Repository
 * @params: actor   ? String -> Name of person who made change
 * @params: changes ? Array  -> Array of commit messages to repo
 */
function bitbucketUpdate(repo,actor,changes){
  return `Bitbucket Update from *${repo}*\nPush from: _${actor}_\n*Changes:*\n${changes.join('')}\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n`
}

/*
 * @params: bundle ? Object -> Containing information about update
 * Replacement for bitbucketUpdate
 */
function bitbucketMessage(bundle) {
  return `Bitbucket Update from *${bundle.repo}*\nPush from: _${bundle.author}_\n*Commits:*\n${bundle.commits.join('')}\n[View](${bundle.link})\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n`
}

/*
 * @params: calendar ? String -> Name of Calendar for Event
 * @params: event    ? JSON   -> Event details from Google Calendar
 */
function calendarEvent(calendar,event){
  return `*New Calendar Event* for _${calendar}_:\n${event}`;
}

/*
 * @params: locationName ? String -> Name of the breached location
 * @params: info         ? JSON   -> Request body from Google Wifi
 */
function locationEntry(locationName,info){
  return `*Entry to: ${locationName}*\nDetails:`;
}

/*
 * @params: report ? Object -> Object containing daily weather
 * Report Object: { 
 *    windDirection: WindDirection,
 *    windSpeed: WindSpeedKph, 
 *    highTemp: HighTempCelsius, 
 *    lowTemp: LowTempCelsius, 
 *    sunriseAt: SunriseAt, 
 *    sunsetAt: SunsetAt, 
 *    todayConditionLink:TodaysConditionImageURL, 
 *    todayCondition: TodaysCondition
 * }
 */
function weatherReport(report) {
  return `*Today's Weather Report:*\n\n  - Conditions: *${report.todayCondition}*\n  - High: *${report.highTemp}*C° | Low: *${report.lowTemp}*C°\n  - Wind: *${report.windSpeed}Kmph ${report.windDirection}*\n\n Have a great day! ☺️`;
}