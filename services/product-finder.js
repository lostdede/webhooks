require('dotenv').config();
const request = require('request-promise');
const puppeteer = require('puppeteer');
const key = process.env.APIKEY || 'something';
const Logs = require('./logger').Log;

async function walmartproductLookUp(barcode) {
  const productURL = `http://api.walmartlabs.com/v1/items?apiKey=${key}&upc=${barcode}`;
  let msg = await request(productURL)
  .then((response, body) => {

    if (body.items.length == 0) {
      Logs.productFinderFail(`No product found for: ${barcode}`);
      return null;
    }

    const resultItem = body.items[0];

    let ourFormattedItem = {
      name: resultItem.name,
      barcode: resultItem.upc,
      images: resultItem.thumbnailImage,
      msrp: resultItem.msrp || 'Not provided',
      brandName: resultItem.brandName,
      addedBy: 'VT Product LookUp',
      updatedAt: new Date().toString(),
      addedAt: new Date().toString()
    }
    return ourFormattedItem;
  })
  .catch((err) =>  Logs.productFinderFail(`${err}`));

  return msg;
}

/* This is what we will be using until walmart starts behaving */
async function phantomLookUp (barcode) {
  const productURL = `https://barcodesdatabase.org/barcode/${barcode}`;
  const browser = await puppeteer.launch({args: ['--no-sandbox']});
  const page = await browser.newPage();

  await page.goto(productURL, {waitUntil: 'networkidle2'});

  const pageContents = await page.content()
  .then(results => {
    let table = results.match(/(<td>).*(<\/td>)+/g);
    if (table.length == 0) {
      return 'Not found';
    }

    table.shift()

    let barcodeData = {
      barcode: barcode,
      name: '',
      images: '',
      msrp: 'Not Provided',
      brandName: '',
      addedBy: 'VT Product Lookup',
      updatedAt: new Date().toString(),
      addedAt: new Date().toString()
    };

    const variableMap = {
      Owner: 'brandName',
      'Product name': 'name',
      'Image url': 'images'
    };

    table.forEach(x => {
      let m = x.replace('</td><td>','%').replace('<td>','').replace('</td>', '');
      let props = m.split('%');

      if (variableMap.hasOwnProperty(props[0])) {
        if (props[0] == 'Image url') {
          barcodeData[variableMap[props[0]]] = props[1].match(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)[0].replace(' class', '').replace('src=', '').replace('"', '');
        } else {
          barcodeData[variableMap[props[0]]] = props[1];
        }
      }
    });
    return barcodeData;
  })
  .catch(err => {
    console.log(err);
    return null;
  })

  browser.close();

  return pageContents;
}

module.exports = phantomLookUp;