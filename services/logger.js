/********************

  Logger for Bot Events.

**********************/


class Logs {
  constructor(showConsole){
    this.botRecords = [];
    this.apiRecords = [];
    this.showConsole = showConsole ? showConsole : false;
  }

  newUser(name){
    let text = `${TimeStamp()} - New User ${name}, has been collected.`;
    this.botRecords.push(text);
    if (this.showConsole){console.log(text);}
  }

  newGroup(title,members){
    members = members ? members : 'Unable to retrieve';
    let text = `${TimeStamp()} - New Group with title: ${name}, has been collected. Number of members: ${members}`;
    this.botRecords.push(text);
    if (this.showConsole){console.log(text);}
  }

  ranCommand(name,command){
    let text = `${TimeStamp()} - User ${name}, just ran the '${command}' command.`;
    this.botRecords.push(text);
    if (this.showConsole){console.log(text);}
  }

  triedCommand(name,command){
    let text = `${TimeStamp()} - User ${name}, just attempted to run the '${command}' command.`;
    this.botRecords.push(text);
    if (this.showConsole){console.log(text);}
  }

  personMessaged(name,message){
    let text = `${TimeStamp()} - Message from ${name}: ${message}`;
    this.botRecords.push(text);
    if (this.showConsole){console.log(text);}
  }

  suggestion(name,message){
    let text = `${TimeStamp()} - Suggestion from ${name}: ${message}`;
    this.botRecords.push(text);
    if (this.showConsole){console.log(text);}
  }

  apiHit(call, ip){
    ip = ip.replace('::ffff:','').replace('::','');
    let text = `${TimeStamp()} - Api call to url: ${call} | IP -  ${ip}`;
    this.apiRecords.push(text);
    if (this.showConsole){console.log(text);}
  }

  codeUpdate(stdout){
    let text = `${TimeStamp()} - Code Base update attempted with following output:\n${stdout}`;
    this.apiRecords.push(text);
    if (this.showConsole){console.log(text);}
  }

  mongoFail(err) {
    let text = `${TimeStamp()} - Mongo Operation Failed with:\n${err}`;
    this.apiRecords.push(text);
    if (this.showConsole){console.log(text);}
  }

  mongoSuccess(msg) {
    let text = `${TimeStamp()} - Mongo Operation Successful:\n${msg}`;
    this.apiRecords.push(text);
    if (this.showConsole){console.log(text);}
  }

  productFound(msg) {
    let text = `${TimeStamp()} - Product Found:\n${msg}`;
    this.apiRecords.push(text);
    if (this.showConsole){console.log(text);}
  }

  productFinderFail(err) {
    let text = `${TimeStamp()} - Product Finder Failed with:\n${err}`;
    this.apiRecords.push(text);
    if (this.showConsole){console.log(text);}
  }
}

const consoleShouldShow = process.env.NODE_ENV == 'prod' ? false : true;
module.exports.Log = new Logs(consoleShouldShow);
module.exports.fieldTimeStamp = fieldTimeStamp;

function fieldTimeStamp(){
  let timed = Timelogger();
  return `_${timed.date.replace(/\//g,'')}_${timed.time}`;
}

function TimeStamp(){
  let timed = Timelogger();
  return `[${timed.date}] ${timed.time}`;
}

function Timelogger(){
  // Make sure to correctly format system time
  function checkdigit(i){
    if (i < 10){
      i = "0" + i;
    }
    return i;
  }
  // format returns a timestamp after recieving the uptime in seconds
  // Date -> String
  function format(seconds){
    function pad(s){
      return (s < 10 ? '0' : '') + s;
    }
    var hours = Math.floor(seconds / (60*60));
    var minutes = Math.floor(seconds % (60*60) / 60);
    var seconds = Math.floor(seconds % 60);

    return pad(hours) + ':' + pad(minutes) + ':' + pad(seconds);
  }

  let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

  var timestamp = new Date();
  var date = timestamp.getFullYear() + "/" +
             checkdigit(months[timestamp.getMonth()]) + "/" + checkdigit(timestamp.getDate());
  var time = checkdigit(timestamp.getHours()) + ":" +
             checkdigit(timestamp.getMinutes()) + ":" +
             checkdigit(timestamp.getSeconds());
  var uptime = format(process.uptime());

 return({date:date,time:time,uptime:uptime});
}
