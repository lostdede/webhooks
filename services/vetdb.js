const ProductFinder = require('./product-finder');
const Logs = require('./logger').Log;

const MongoClient = require('mongodb').MongoClient;
const URL = process.env.MONGO_URL || 'mongodb://172.17.0.2:27017/';

class DataBase {

  constructor() {
    MongoClient.connect(URL, { useNewUrlParser: true })
      .then((db) => {
        this.DB = db.db('vt-products');
        this.Collection = this.DB.collection('products');
      })
      .catch((err) => {
        throw err;
      })
  }

  async getAllEntries() {
    let search = await this.Collection.find({}).toArray()
    .then(result => {
      return result;
    })

    return search;
  }

  async getItemCount() {
    let search = await this.Collection.countDocuments({})
    .then(result => {
      return result
    })

    return search;
  }

  async getIncompleteItemsCount() {
    let search = await this.Collection.countDocuments({name: ""})
    .then(result => {
      return result;
    })

    return search;
  }

  async getRecentEntries(no = 5) {
    let search = await this.Collection.find().sort({_id:-1}).limit(no).toArray()
    .then(results => {
      return results;
    })

    return search;
  }

  // Check if an Item exists in our DB
  async doesExist(barcode) {
    const query = {barcode: barcode};

    async function handleVerdict (result) {
      if (result && result.length != 0) {
        return result[0];
      } else {
        let item = await ProductFinder(barcode)
        .then(result => {
          return result;
        })
        .catch (err => {
          console.log(err)
          return false;
        });
        return item;
      }
    }

    let verdict = await this.Collection.find(query).toArray()
    .then(result => handleVerdict(result, this.insertOne));

    if (verdict) {
      this.insertOne(verdict);
    }

    return verdict;
  }

  async insertMany(arr) {
    let data = arr.map(itm => {
        itm.barcode = itm.id;
        itm.name = itm.name ? itm.name : "";
        itm.brandName = itm.brandName ? itm.brandName : "";
        itm.images = itm.images ? itm.images : "";
        itm.msrp = itm.msrp ? itm.msrp : 'Not provided';
        itm.addedBy = itm.addedBy ? itm.addedBy : 'Not recorded';
        itm.addedAt = itm.addedAt ? itm.addedAt : new Date().toString();
        itm.updatedAt = itm.updatedAt ? itm.updatedAt : new Date().toString();
        delete itm.id;
        delete itm.quantity;
        return itm;
    });

    let insert = await this.Collection.insertMany(data)
    .then(result => {
      if (result) {
        return {success: true, data: result};
      } else {
        return {success: false, data: null};
      }
    });

    return insert;
  }

  async insertOne(itm) {
    itm.barcode = itm.id || itm.barcode;
    itm.name = itm.name ? itm.name : "";
    itm.brandName = itm.brandName ? itm.brandName : "";
    itm.images = itm.images ? itm.images : "";
    itm.msrp = itm.msrp ? itm.msrp : 'Not provided';
    itm.addedBy = itm.addedBy ? itm.addedBy : 'Not recorded';
    itm.addedAt = itm.addedAt ? itm.addedAt : new Date().toString();
    itm.updatedAt = itm.updatedAt ? itm.updatedAt : new Date().toString();
    delete itm.id;
    delete itm.quantity;

    const query = { barcode: itm.barcode };

    let queryResult = await this.Collection.find(query).toArray()
    .then(result => {
      if (result && result.length != 0) {
        return true;
      } else {
        return false;
      }
    });

    if (!queryResult) {
      let res = this.Collection.insertOne(itm).then(result => {
        if (result) {
          return {sucess: true, data: result};
        } else {
          return {success: false, data: null};
        }
      })
      return res;
    } else {
      return {success: true, data: this.doesExist(itm.barcode)};
    }
  }


  async updateItem(barcode, data) {
    delete data.id;
    this.Collection.updateOne({barcode: barcode}, {'$set': data})
    .then(result => {
      Logs.mongoSuccess(`Item updated: ${barcode}`);
      return result;
    })
    .catch(err => {
      Logs.mongoFail(`Item update failed: ${barcode}\n Details: ${err}`);
    })
  }

  async deleteItem(barcode) {
    this.Collection.deleteOne({barcode: barcode})
    .then(result => {
      Logs.mongoSuccess(`Item Deleted: ${barcode}`);
      return result;
    })
    .catch(err => {
      Logs.mongoFail(`Item delete failed: ${barcode}\n Details: ${err}`);
    })
  }
}

module.exports = new DataBase();