/*
  Abstract most bot functions and extend some of them
*/
require('dotenv').config();
const TeleBot = require('telebot');
const bot = new TeleBot(process.env.ANABOT);
const path = require('path');


/* Bot Handler Functions */

/*
 * sendRegMessage will send a message formatted with the default
 * @params id - Username or telegram id to send the response to
 * @params msg - Response to send to id
 */
function sendRegMessage(id,msg){
  return bot.sendMessage(id,msg);
}

/*
 * sendMarkdownMessage will send a message formatted with Markdown
 * @params id - Username or telegram id to send the response to
 * @params msg - Response to send to id
 */
function sendMarkdownMessage(id,msg){
  return bot.sendMessage(id,msg,{parseMode:'Markdown'});
}

/*
 * sendHTMLMessage will send a message formatted with html
 * @params id - Username or telegram id to send the response to
 * @params msg - Response to send to id
 */
function sendHTMLMessage(id,msg){
  return bot.sendMessage(id,msg,{parseMode:'HTML'});
}

/* sendPrivateMessage will send a response back to the sender in their personal chat with the bot
 * @params msg {Telebot msg Object}
 * @params res {String} - Response message to be sent
 */
function sendPrivateMessage(msg,res,format = 'Markdown'){
  switch (format) {
    case 'Markdown':
      sendMarkdownMessage(msg.from.id,res);
      break;
    case 'HTML':
      sendHTMLMessage(msg.from.id,res);
      break;
    default:
      sendMarkdownMessage(msg.from.id,res);
      break;
  }
}

/* sendReply will send a response back to the sender in the chat they sent the message from
 * @params msg {Telebot msg Object}
 * @params res {String} - Response message to be sent
 */
function sendReply(msg,res,format = 'Markdown'){
  switch (format) {
    case 'Markdown':
      sendMarkdownMessage(msg.chat.id,res);
      break;
    case 'HTML':
      sendHTMLMessage(msg.chat.id,res);
      break;
    default:
      sendMarkdownMessage(msg.chat.id,res);
      break;
  }
}

/* sendPhoto will send a message with a photo to the desired reciever
 * @params id {Int} - Person to send the message to
 * @params url {valid url to image}
 * @params msg {String} - Photo Caption to be sent. Markdown by default
 */
function sendPhoto(id,url, msg) {
  return bot.sendPhoto(id, url, {caption: msg, parseMode: 'Markdown'});
}

/* Send photo to adventure group */
function sendAdventureGroupPhoto(url, msg) {
  return bot.sendPhoto(process.env.ADVENTURE, url, {caption: msg, parseMode: 'Markdown'});
}

/*  Send Message to Adventure Time Group */
function sendAdventureGroupMessage(msg){
	sendMarkdownMessage(process.env.ADVENTURE,msg);
}


module.exports.botHandler = {
  sendRegMessage:sendRegMessage,
  sendMarkdownMessage:sendMarkdownMessage,
  sendHTMLMessage:sendHTMLMessage,
  sendPrivateMessage:sendPrivateMessage,
  sendReply: sendReply,
  sendAdventureGroupMessage: sendAdventureGroupMessage,
  sendPhoto:sendPhoto,
  sendAdventureGroupPhoto: sendAdventureGroupPhoto
};
